import Vue from 'vue'
import Vuex from 'vuex'
import platformAPIs from '../apis/platformAPIs'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    platformData: []
  },
  mutations: {
    updatePlatforms(state, platformsData) {
      state.platformData = Object.assign({}, platformsData);
    }
  },
  actions: {
    async fetchPlatforms({ commit }) {
      const platformsData = await platformAPIs.getPlatformData();
      commit("updatePlatforms", platformsData);
    }
  },
  modules: {},
  
  getters: {
    platformDetails: state => {
      return state.platformData[0];
    },
    registrationDetails: (state, getters) => {
      return getters.platformDetails["registrations"];
    }
  }
});
