import axios from 'axios'

export default {
    async getPlatformData () {
        const response = await axios.get("/bins/az5ig");
        return response.data
    }
}