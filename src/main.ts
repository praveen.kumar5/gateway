import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store/platformStore'
import axios from 'axios'
import appConfig from './../Configs/app-config'
import { DropdownPlugin } from "bootstrap-vue";

Vue.use(BootstrapVue)
Vue.use(DropdownPlugin)

import './assets/scss/_helpers.scss'

axios.defaults.baseURL = appConfig.appSettings["api-base-url"];

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
