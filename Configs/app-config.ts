export default {
    'appSettings': {
        'app-title': 'LTI 1.3 Gateway',
        'app-subTitle': 'Reference Implementation',
        'app-description': 'This is a sample implementation of LTI 1.3 Gateway specification. Please select a PLATFORM and TOOL from the dropdown list below to view its out-of-band registration configuration.',
        'app-copyRightText': 'Compro Technologies Pvt Ltd',
        'api-base-url': 'https://api.myjson.com/'
    }
}